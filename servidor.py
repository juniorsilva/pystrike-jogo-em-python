'''
Created on 17/02/2014

@author: JuniorSilva

'''

#!/usr/bin/env python3
# arquivo: servidor.py
# -*- coding:utf-8 -*-

from socket import *
import threading, turtle, jogador
 
## Class que irá criar a Thread e realizar a comunicação ##
class ClientThread(threading.Thread):
    ## nstancía ##    
    def __init__(self, ip, port, socket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket
        self.jogador = ''
        print ("[+] New thread started for ",ip,":",str(port))

    ## Realiza a comunicação ##
    def run(self):
        bem_vindo = 'Bem vindo ao PyStrike'
        self.socket.send(bytearray(bem_vindo, 'utf-8'))
        time = 'Escolha o time: 1 para CT ou 2 para Terror'
        self.socket.send(bytearray(time, 'utf-8'))
        time = self.socket.recv(1024)
        time = time.decode('utf-8')
        nome = 'Digite seu nome:'
        self.socket.send(bytearray(nome, 'utf-8'))
        nome = self.socket.recv(1024)
        nome = nome.decode('utf-8')        
        nome = jogador.Jogador(nome, time)
        self.jogador = nome        
        while True:
            acao = '''
            f (valor) - Frente
            v (valor) - Tras
            d (valor) - Direita
            e (valor) - Esquerda
            a - Atirar
            'vazio' - Sair do jogo
            '''
            self.socket.send(bytearray(acao, 'utf-8'))
            acao = self.socket.recv(1024)
            acao = acao.decode('utf-8')                      
            if acao != "":
                if acao != 'a' and acao != 'atirar':
                    nome.mover(acao)                    
                elif acao == 'a' or acao == 'atirar':
                    posicao = nome._imagem.pos()                    
                    x = posicao[0]
                    y = posicao[1]
                    ang = nome._imagem.heading()
                    nome.atirar(x, y, ang, threads)
                else:
                    msg = 'Não entendi!'
                    self.socket.send(bytearray(msg, 'utf-8'))
            else:
                turtle.bye()
            
        if nome._atingido == True:
            msg_at = 'Você foi atingido'
            self.socket.send(bytearray(msg_at, 'utf-8'))
            
      
            

host = ""
port = 2000

socketServidor = socket(AF_INET, SOCK_STREAM)
socketServidor.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

socketServidor.bind((host,port))
threads = []
nomes = []
tela = turtle.Screen()
tela.bgpic('imagens/gramado.gif')
tela.setup(1000,600)
var = 1
while var <= 2:
    socketServidor.listen(4)
    print ("##### Esperando por conexões #####")
    (conexao, (end_ip, num_porta)) = socketServidor.accept()
    newthread = ClientThread(end_ip, num_porta, conexao)
    newthread.start()
    threads.append(newthread)
    var += 1
    
tela.mainloop()
