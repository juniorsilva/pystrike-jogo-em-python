#!/usr/bin/env python3
# -*- coding:utf-8 -*-
 
from socket import *
 
HOSPEDEIRO = input('Servidor (nome ou IP): ')
PORTA = int(input('Porta: '))
 
clienteSocket = socket(AF_INET, SOCK_STREAM)
clienteSocket.connect((HOSPEDEIRO, PORTA))
 
mensagem = clienteSocket.recv(1024)
dados_recebido = mensagem.decode('utf-8')   
print ('Recebido:', dados_recebido)
while mensagem != '':
    dados_recebido = clienteSocket.recv(1024)
    dados_recebido = dados_recebido.decode('utf-8')   
    print ('Recebido:', dados_recebido)
    mensagem = input('Resposta: ')
    dados_a_enviar = bytearray(mensagem, 'utf-8')
    clienteSocket.send(dados_a_enviar)
clienteSocket.close()  