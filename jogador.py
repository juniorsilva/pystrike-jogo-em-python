'''
Created on 17/02/2014

@author: JuniorSilva

'''

#!/usr/bin/env python3
# arquivo: servidor.py
# -*- conding: utf-8 -*-

from socket import *
import turtle, string


class Jogador():
    ####  Inicia o objeto ####
    def __init__(self, nome:str, time):
        self._nome = nome
        self._imagem = turtle.Turtle()
        self._configure_imagem(time)
        self._bala = Bala()
        self._atingido = False
    #### Configura a imagem do objeto ####
    def _configure_imagem(self, time):
        self._imagem.shape('turtle')
        self._imagem.up()        
        if(time == '1'):
            self._imagem.setposition(-400, 200)
        elif(time == '2'):
            self._imagem.color('red')
            self._imagem.setposition(400, -200)
            self._imagem.rt(180)
        

    def mover(self, direcao):
        texto = direcao.split(" ")
        num = int(texto[1])
        direcao = texto[0]
        if direcao == "frente" or direcao == "f":
            self._imagem.fd(num)
        elif direcao == "voltar" or direcao == "v":
            self._imagem.bk(num)
        elif direcao == "direita" or direcao == "d":
            self._imagem.rt(num)
        elif direcao == "esquerda" or direcao == "e":
            self._imagem.lt(num)
        else:
            print('Não entendi, repita.')

    def atirar(self, x, y, ang, threads):        
        self._bala._imagem.seth(ang)
        self._bala._imagem.setpos(x,y)
        self._bala._imagem.st()        
        cont = 1
        #Nessa parte esta um daley infinito, mas noo sei resolver
        while cont <= 1000:
            self._bala._imagem.fd(1)            
            posicao_tiro = self._bala._imagem.pos()            
            colisao = detectarColisao(posicao_tiro, threads)    
            cont += 1            
            if(colisao == 1):
                break
        self._bala._imagem.ht()
        
         
def detectarColisao(posicao_tiro, threads):    
    acertou = False
    for th in threads:        
        posicao_jog = th.jogador._imagem.pos()
        x_jog = posicao_jog[0]
        x_tiro = posicao_tiro[0]
        y_jog = posicao_jog[1]
        y_tiro = posicao_tiro[1]
        print('Jogador: ', x_jog)
        print('Tiro: ', x_tiro)
        if(x_jog == x_tiro and y_jog == y_tiro):            
            acertou = True
            th.jogador._atingido = True
            print('Acertou: ', acertou)            
            print('jogador ',th.jogador._nome ,'foi atingido.')            
            break
    
    if acertou == True:        
        return 1
    else:        
        return 0
            
    
        

class Bala():
    def __init__(self):
       self._imagem = turtle.Turtle()
       self._configure_imagem()

    def _configure_imagem(self):
        self._imagem.shape('circle')
        #self._imagem.shapesize(1, 3)
        self._imagem.up()
        self._imagem.ht()

    
        
        
        
       
